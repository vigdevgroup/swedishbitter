window.onload = function() {

 // ANATOMY
 let list = document.querySelector('.anatomy__list');


 list.onclick = function() {

     let target = event.target.closest('.item');

     if (!target) return;

     let marker = document.querySelector('.marker');

     marker.id = 'item-' + target.dataset.id;
     marker.querySelector('.marker__inner').innerHTML = (target.dataset.id < 10) ? '0' + target.dataset.id : target.dataset.id;
 };

 document.querySelector('.anatomy__layout-link').onclick = function (){
     event.preventDefault();
     document.querySelector('.reviews').scrollIntoView({
        behavior: "smooth",
        block: "start"
     });
 }

 // /ANATOMY

//HISTORY

 function checkWidth() {
     (document.documentElement.clientWidth <= 600) ?
     document.querySelector('.history').style.paddingBottom = document.querySelector('.history__bottom').offsetHeight + 10 + 'px': document.querySelector('.history').style.paddingBottom = '';

     setTimeout(() => checkWidth(), 1000);
 }

 checkWidth();

 let comp = document.querySelector('.composition__inner');
 let item = null;

 // /HISTORY

 // COMPOSITION
 comp.onmouseover = function() {

     if (item) return;

     item = event.target.closest('.composition__item-inner');

     if (!item) return;

     item.querySelector('.composition__description').style.display = 'block';
 }

 comp.onmouseout = function() {

     if (!item) return;

     if (item.contains(event.relatedTarget)) return;

     item.querySelector('.composition__description').style.display = '';
     item = null;

 }

 // /COMPOSITION

 //REVIEWS
 let reviews = document.querySelector('.reviews');

 reviews.addEventListener('click', showTabs);
 document.querySelector('.modal-reviews').addEventListener('click', showTabs)

 function showTabs() {
      target = event.target.closest('.reviews__tab');

     if (!target) return;

    let revWrapper = target.closest('.reviews__wrapper');
    let foreign = revWrapper.querySelector('#foreign');
    let russian = revWrapper.querySelector('#russian');

     if (target.dataset.id == 'russian') {
        revWrapper.querySelector('.reviews__tab[data-id = "foreign"]').classList.remove('active');
         target.classList.add('active');
         foreign.style.display = 'none';
         foreign.classList.remove('showed');
         foreign.classList.add('hidden');
         russian.style.display = 'flex';
         russian.classList.remove('hidden');
         russian.classList.add('showed');
     } else if (target.dataset.id == 'foreign') {
        revWrapper.querySelector('.reviews__tab[data-id = "russian"]').classList.remove('active');
         target.classList.add('active');
         russian.style.display = 'none';
         russian.classList.remove('showed');
         russian.classList.add('hidden');
         foreign.style.display = 'flex';
         foreign.classList.remove('hidden');
         foreign.classList.add('showed');
     }
 }
 // /REVIEWS

 //MODAL-REVIEWS

 let mRevView = document.querySelector('.modal-reviews .modal-view');
 let content;

 mRevView.onscroll = function (){
     content = mRevView.querySelector('.reviews__block.showed .block__main-content').innerHTML;
     if (mRevView.scrollHeight - mRevView.scrollTop - mRevView.clientHeight < 5){
         mRevView.querySelector('.reviews__block.showed .block__additional-content').insertAdjacentHTML('beforeend', content);
     }
 }

 // /MODAL-REVIEWS

 // MAIN
 document.addEventListener('click', function(){
    let target = event.target.closest('a.default__btn') || event.target.closest('.reviews__link');

    if (!target) return;

    event.preventDefault();

    let modal;

    if (target.classList.contains('reviews__link')){
        modal = document.querySelector('.modal-reviews');
        modal.querySelector('.modal-view').style.overflow = 'auto';
    }
    else{
        if (target.classList.contains('more__btn-mobile')){
            modal = document.querySelector('.modal-more');
        }
        else {
            modal = document.querySelector('.modal');
        }
    }
    

    modal.style.display = 'flex';
    modal.classList.add('modal-on');
    modal.classList.remove('modal-off');
    document.body.style.overflowY = 'hidden';
    let firstElem = document.forms.order.elements[0];
    let lastElem = document.forms.order.elements[document.forms.order.elements.length - 1];
    firstElem.focus();

    firstElem.onkeydown = function (){
        if (event.key == 'Tab' && event.shiftKey){
            lastElem.focus();
            return false;
        }
    }

    lastElem.onkeydown = function (){
        if(event.key == 'Tab' && !event.shiftKey){
            firstElem.focus();
            return false;
        }
    }

    document.onkeydown = function (){
        if (event.key == 'Escape'){
            closeModal();
        }
    }

    modal.onclick = function (){
        let target = event.target;
        if (!(target.closest('.close') || target.closest('.modal-cover'))) return;

        closeModal();
    }

    function closeModal (){
        modal.classList.add('modal-off');
        modal.classList.remove('modal-on');
        lastElem.onkeydown = firstElem.onkeydown = document.onkeydown = null;
        setTimeout(() => {
            modal.style.display = '';
            let blocks = mRevView.querySelectorAll('.reviews__block');
            for (let block of blocks){
                block.querySelector('.block__additional-content').innerHTML = '';
            }
        }, 500);
        document.body.style.overflowY = '';
    }
});

document.addEventListener('submit', function(){
    event.preventDefault();

    let target = event.target;
    let form = target.closest('form');

    let data = {};
    let errs = [];
    if (form.name){
        if (!form.nameValid){
            errs.push(form.name.closest('.input-wrapper'));
        } else {
            data.name = form.name.value;
        }
    }
    if (form.phone){
        if (!form.phoneValid){
            errs.push(form.phone.closest('.input-wrapper'));
        } else{
            data.phone = form.phone.value;
        }
    }
    if (form.email){
        if (!form.emailValid){
            errs.push(form.email.closest('.input-wrapper'));
        } else{
            data.email = form.email.value;
        }
    }

    if (errs.length != 0){
        for (err of errs){
            err.classList.remove('success');
            err.classList.add('error');
        }
        return;
    }

    data.subject = form.dataset.subject;

    send(data);
    
    function send (){
        let message = ''

        if (data.subject == 'order'){
            message = 'Ваша заявка успешно поступила. Мы свяжемся с вами в близжайшее время для оформления заказа!';
        } else if (data.subject == 'subcription'){
            message = 'Подписка оформлена! Теперь вы будете автоматически получать свежие новости на свою почту.';
        } else if (data.subject == 'toLearnMore'){
            message = 'Ваша заявка успешно поступила! Мы перезвоним вам в близжайшее время.';
        }

        showNotice(message);
    }

    function showNotice (message, err){
        let notice = document.querySelector('.notice');

        notice.style.display = 'block';

        if (err)
            notice.style.color = '#E74C3C';
        else 
            notice.style.color = '';

        notice.querySelector('.notice__text').innerHTML = message;
        notice.classList.remove('hide');
        notice.classList.add('show');
        setTimeout(() => {
            notice.classList.remove('show');
            notice.classList.add('hide');
            setTimeout(() => {
                notice.querySelector('.notice__text').innerHTML = '';
                notice.style.display = '';
            }, 1000);
        }, 4000)
    }
    
});

let phonePattern = /^\+7\s\(\d\d\d\)\s\d\d\d-\d\d-\d\d$/;
document.addEventListener('focusin', function (){
    let target = event.target;

    if (!target.closest('input[name="phone"]')) return;

    if (!phonePattern.test(target.value)){
        target.value = '+7 (';
        target.closest('form').phoneValid = false;
        target.onblur = function (){
            if (!phonePattern.test(target.value)){
                this.closest('.input-wrapper').classList.remove('success');
                this.closest('.input-wrapper').classList.add('error');
            }
            this.onblur = null;
        }
    }
});

document.addEventListener('input', function (){
    let target = event.target;

    if (!target.closest('input[name="phone"]')) return;
     
    if (target.value.length == 7){
        target.value += ') ';
    } else if (target.value.length == 12){
        target.value += '-';
    } else if (target.value.length == 15){
        target.value += '-';
    }
});

document.addEventListener('keydown', function (){
    if (!event.target.closest('input[name="phone"]')) return;

    if (!(/\d/.test(event.key) || event.key == 'Backspace' || event.key == 'ArrowRight' || event.key == 'ArrowLeft' || event.key == 'Delete' || event.key == 'Tab' || event.key == 'Enter'))
        event.preventDefault();
});

document.addEventListener('change', function (){
    let target = event.target;
    let form = target.closest('form');
    let input = target.closest('.input-wrapper');

    if (target.name == 'email'){
        let regexp = /^[\w-.]+@([-\w]+\.)+[-\w]+$/;

        if (regexp.test(target.value)){
            form.emailValid = true;
            input.classList.remove('error');
            input.classList.add('success');
        }
        else {
            form.emailValid = false;
            input.classList.remove('success');
            input.classList.add('error');
        }
    } else if (target.name == 'phone'){
        if (phonePattern.test(target.value)){
            form.phoneValid = true;
            input.classList.remove('error');
            input.classList.add('success');
        }
        else {
            form.phoneValid = false;
            input.classList.remove('success');
            input.classList.add('error');
        }
    } else if (target.name == 'name'){
        let regexp = /^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я]+$/;

        if (regexp.test(target.value)){
            form.nameValid = true;
            input.classList.remove('error');
            input.classList.add('success');
        }
        else {
            form.nameValid = false;
            input.classList.remove('success');
            input.classList.add('error');
        }
    }
});

// /MAIN

}